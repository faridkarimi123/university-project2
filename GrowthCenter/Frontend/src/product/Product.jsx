import React from 'react'
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import ProductInfo from '../components/Product'


function Product() {
  return (
    <>
        <div>
            <Navbar />
            <ProductInfo />
            <Footer />
        </div>
    </>
  )
}

export default Product