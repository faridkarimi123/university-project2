import React from "react";
import Navbar from "../components/Navbar";
import Banner from "../components/Banner";
import Freebook from "../components/Freebook";
import Footer from "../components/Footer";
import Photo from "../components/Photo";

function Home() {
  return (
    <>
      <Navbar />
      <Banner />
      <Freebook />
      <br />
      <br />
      <Photo />
      <Footer />
    </>
  );
}

export default Home;
