import React from 'react'
import Navbar from "../components/Navbar";
import Memberss from "../components/Members"
import Footer from "../components/Footer";

function Members() {
    return (
        <>
            <div>
                <Navbar />
                <div className=" min-h-screen">
                    <Memberss />
                </div>
                <Footer />
            </div>
        </>
    )
}

export default Members