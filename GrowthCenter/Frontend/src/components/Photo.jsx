import React from 'react'

function Photo() {
    return (
        <>
            <div className='max-w-screen-2xl container mx-auto md:px-20 px-4'>
                <div className="carousel carousel-center rounded-box items-center justify-center ml-80 w-full gap-3 mb-10">
                    <div className="carousel-item ">
                        <img src="https://armangar.com/images/school/fani.png" alt="Drink" className='w-80 rounded-md' />
                    </div>
                    <div className="carousel-item">
                        <img src="/p1.jpg" alt="Drink" className='w-80 h-80 rounded-md' />
                    </div>
                    <div className="carousel-item">
                        <img src="/p2.jpg" className='w-80  h-80 rounded-md' alt="Drink" />
                    </div>
                    
                </div>
            </div>
        </>
    )
}

export default Photo