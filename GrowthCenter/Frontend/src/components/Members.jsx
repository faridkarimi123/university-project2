import React, { useEffect, useState } from 'react';
import Cards from "./MemCards"
// import axios from "axios";
import { Link } from 'react-router-dom';
import memList from '../../public/memList.json'
function Members() {
  const filterData = memList.filter((data) => data.s === "org")
  return (
    <>
      <div className=" max-w-screen-2xl container mx-auto md:px-20 px-4">
        <div className="mt-28 items-center justify-center text-center">
          <h1 className="text-2xl  md:text-4xl">
            We're delighted to have you{" "}
            <span className="text-pink-500"> Here! :)</span>
          </h1>
          <p className="mt-12">
            In this section, you can view and purchase all free and non-free courses. Also, we tried to update the courses for you in this section so that the students of Kermanshah No. 1 Technical and Vocational University of Kermanshah can improve. We hope that it can help and make you learn, dear students.
          </p>
          <Link to="/">
            <button className="mt-6 bg-pink-500 text-white px-4 py-2 rounded-md hover:bg-pink-700 duration-300">
              Back to home Page <i class='bx bxs-home-heart'></i>
            </button>
          </Link>
        </div>
        <div className="mt-12 grid grid-cols-1 md:grid-cols-3">
          {filterData.map((item) => (
            <Cards key={item.id} item={item} />
          ))}
        </div>
      </div>
    </>
  )
}

export default Members