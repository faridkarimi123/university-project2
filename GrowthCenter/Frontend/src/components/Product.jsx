import React from 'react'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Product() {
    const tt = localStorage.getItem("Product")
    const ff = JSON.parse(tt);
    const notify = () => toast("✅ Success your Product");
    return (
        <>
            <div className='max-w-screen-2xl container mx-auto md:px-20 px-4 flex flex-col md:flex-row my-10'>
                <div className='flex flex-col justify-between lg:flex-row gap-16 lg:items-center'>
                    <div className='flex flex-col gap-6 lg:w-2/4 md:mt-36'>
                        <img src={ff.image} alt={ff.name} className='md:w-[450px] md:h-[600px] rounded-md cursor-pointer' />
                    </div>
                    <div className='flex flex-col gap-4 lg:w-2/4'>
                        <div>
                            <h1 className='text-3xl font-bold'>{ff.name}</h1>
                            <span className=' text-violet-600 font-semibold'>{ff.title}</span>
                        </div>
                        <h6 className='text-2xl font-semibold'>$ {ff.price}</h6>
                        <div className='flex flex-row items-center gap-12'>
                            <button className="bg-violet-800 text-white font-semibold py-3 px-16 rounded-xl ">Buy {ff.name} Course</button>
                        </div>
                        <div className="stats shadow">

                            <div className="stat">
                                <div className="stat-figure text-secondary">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="inline-block w-8 h-8 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                                </div>
                                <div className="stat-title">Students</div>
                                <div className="stat-value">31K</div>
                                <div className="stat-desc">Jan 1st - Feb 1st</div>
                            </div>

                            <div className="stat">
                                <div className="stat-figure text-secondary">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="inline-block w-8 h-8 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 6V4m0 2a2 2 0 100 4m0-4a2 2 0 110 4m-6 8a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4m6 6v10m6-2a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4"></path></svg>
                                </div>
                                <div className="stat-title">New Users</div>
                                <div className="stat-value">4,200</div>
                                <div className="stat-desc">↗︎ 400 (22%)</div>
                            </div>

                            <div className="stat">
                                <div className="stat-figure text-secondary">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="inline-block w-8 h-8 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 8h14M5 8a2 2 0 110-4h14a2 2 0 110 4M5 8v10a2 2 0 002 2h10a2 2 0 002-2V8m-9 4h4"></path></svg>
                                </div>
                                <div className="stat-title">New Registers</div>
                                <div className="stat-value">1,200</div>
                                <div className="stat-desc">↘︎ 90 (14%)</div>
                            </div>

                        </div>
                        <div className="mockup-code">
                            <pre data-prefix="$" className='text-warning'>
                                <code>
                                    echo "GrowthCenter4"
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Product