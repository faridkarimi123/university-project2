import express, { json } from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import cors from "cors";
import bookRoute from "./route/book.route.js";
import userRoute from "./route/user.route.js";
// const fs = require('fs')
// const memJson = require('./public/mem.list.json')
const app = express();
// const corsOptions = {
//     origin: ['http://localhost:5173/', 'http://localhost:5174/'],
//     optionsSuccessStatus: 200
// }
app.use(express.static('public'))
app.use(cors());
app.use(express.json());

dotenv.config();

const PORT = process.env.PORT || 4000;
const URI = process.env.MongoDBURI;

// connect to mongoDB
try {
    mongoose.connect(URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
    console.log("Connected to mongoDB");
} catch (error) {
    console.log("Error: ", error);
}

// defining routes
app.use("/book", bookRoute);
app.use("/user", userRoute);
// app.use("/mem", userRoute);

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});